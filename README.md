This repo contains the specification document for Z-Wave TAP DLT 297.

View the text online [HERE](zwave-g9959-tap.adoc)

Download the PDF [HERE](zwave-g9959-tap.adoc)
