
INPUT_FILES := zwave-g9959-tap.adoc

OUTPUT_FILES = $(INPUT_FILES:.adoc=.pdf)

.PHONY: pdf
pdf: $(OUTPUT_FILES)

%.pdf: %.adoc
	asciidoctor-pdf $^ -o $@

clean:
	rm -f $(OUTPUT_FILES)
